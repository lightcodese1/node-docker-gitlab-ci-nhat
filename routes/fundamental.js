const express = require('express');
const router = express.Router();

require('dotenv').config();
const variableData = process.env.variableData || 'Fundamentals';

router.get('/', function (req, res, next) {
  res.send({
    name: 'Fundamentals',
    server: 'express',
    variableData: variableData,
  });
});

// 1
function sumTwoNumber(a, b) {
  return a + b;
}
router.get('/sumTwoNumber', function (req, res, next) {
  res.send(sumTwoNumber(1, 2));
});
// 2

function isEqual(a, b) {
  return a === b;
}
router.get('/isEqual', function (req, res, next) {
  res.send(isEqual(1, 1));
});
// 3

function typeOfValue(a) {
  return typeof a;
}
router.get('/typeOfValue', function (req, res, next) {
  res.send(typeOfValue('a'));
});
// 4

function printValueOfIndex(a, n) {
  return a[n - 1];
}
router.get('/printValueOfIndex', function (req, res, next) {
  res.send(printValueOfIndex('abc', 1));
});
// 5

function removeThreeFirst(a) {
  return a.slice(3);
}
router.get('/removeThreeFirst', function (req, res, next) {
  res.send(removeThreeFirst('abcdefg'));
});

// 6
function getLastThree(str) {
  return str.slice(-3);
}
router.get('/getLastThree', function (req, res, next) {
  res.send(getLastThree('abcdefg'));
});
// 7
function getThreeFirst(a) {
  return a.slice(0, 3);
}
router.get('/getThreeFirst', function (req, res, next) {
  res.send(getThreeFirst('abcdefg'));
});
// 8
function getIdxStr(a) {
  return a.indexOf('is');
}
router.get('/getIdxStr', function (req, res, next) {
  res.send(getIdxStr('asisas'));
});
// 9
function getHalfLength(a) {
  return a.slice(0, a.length / 2);
}
router.get('/getHalfLength', function (req, res, next) {
  res.send(getHalfLength('asisas'));
});
// 10
function removeLastNChar(a) {
  return a.slice(0, -3);
}
router.get('/removeLastNChar', function (req, res, next) {
  res.send(removeLastNChar('asisas'));
});
// 11
function returnPercentage(a, b) {
  return (b / 100) * a;
}
router.get('/returnPercentage', function (req, res, next) {
  res.send(returnPercentage(10, 50));
});
// 12
function mathOperator(a, b, c, d, e, f) {
  return (((a + b - c) * d) / e) ** f;
}
router.get('/mathOperator', function (req, res, next) {
  res.send(mathOperator(1, 2, 3, 4, 5, 6));
});
// 13
function catString(a, b) {
  return a.indexOf(b) === -1 ? a + b : b + a;
}
router.get('/catString', function (req, res, next) {
  res.send(catString(1, 2, 3, 4, 5, 6));
});
// 14
function checkEven(a) {
  return a % 2 === 0;
}
router.get('/checkEven', function (req, res, next) {
  res.send(checkEven(6));
});
// 15
function countTimes(a, b) {
  return b.split(a).length - 1;
}
router.get('/countTimes', function (req, res, next) {
  res.send(countTimes('h', 'hello hi nhat'));
});
// 16
function checkWholeNum(a) {
  return a - Math.floor(a) === 0;
}
router.get('/checkWholeNum', function (req, res, next) {
  res.send(checkWholeNum(12));
});
// 17
function mulAndDiv(a, b) {
  return a < b ? a / b : a * b;
}
router.get('/mulAndDiv', function (req, res, next) {
  res.send(mulAndDiv(12, 6));
});
// 18
function roundNum(a) {
  return Number(a.toFixed(2));
}
router.get('/roundNum', function (req, res, next) {
  res.send(roundNum(13.5213123));
});
// 19
function splitNum(a) {
  const string = a + '';
  const strings = string.split('');
  return strings.map((digit) => Number(digit));
}
router.get('/splitNum', function (req, res, next) {
  res.send(splitNum(1234567));
});

module.exports = router;
